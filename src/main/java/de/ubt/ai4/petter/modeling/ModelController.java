package de.ubt.ai4.petter.modeling;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/model/")
@AllArgsConstructor
public class ModelController {

    private ModelService service;

    @GetMapping("getModelByName")
    public void getModelByName(@RequestParam String modelId) {
        service.getModel(modelId);
    }

}
