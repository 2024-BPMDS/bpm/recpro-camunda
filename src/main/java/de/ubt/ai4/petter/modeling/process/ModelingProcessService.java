package de.ubt.ai4.petter.modeling.process;

import de.ubt.ai4.petter.model.BpmsProcess;
import lombok.AllArgsConstructor;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ModelingProcessService {

    private RepositoryService repositoryService;

    public List<BpmsProcess> getAll() {
        List<ProcessDefinition> processDefinitions = repositoryService.createProcessDefinitionQuery().list();
        List<ProcessDefinition> latest = repositoryService.createProcessDefinitionQuery().latestVersion().list();
        return BpmsProcess.fromProcessList(processDefinitions, repositoryService, latest);
    }

    public List<BpmsProcess> getLatest() {
        List<ProcessDefinition> processDefinitions = repositoryService.createProcessDefinitionQuery().latestVersion().list();
        return BpmsProcess.fromProcessList(processDefinitions, repositoryService, processDefinitions);
    }
}
