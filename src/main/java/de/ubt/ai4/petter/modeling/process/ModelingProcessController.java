package de.ubt.ai4.petter.modeling.process;

import de.ubt.ai4.petter.model.BpmsProcess;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/modeling/process/")
@AllArgsConstructor
public class ModelingProcessController {

    private ModelingProcessService processService;

    @GetMapping("getAll")
    public List<BpmsProcess> getAll() {
        return processService.getAll();
    }

    @GetMapping("getLatest")
    public List<BpmsProcess> getLatest() {
        return processService.getLatest();
    }
}
