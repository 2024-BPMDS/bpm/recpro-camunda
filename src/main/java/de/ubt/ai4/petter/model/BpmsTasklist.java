package de.ubt.ai4.petter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class BpmsTasklist {

    private Long id;
    private String assignee;
    private List<BpmsTask> tasks;

}
