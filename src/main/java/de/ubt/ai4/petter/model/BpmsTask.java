package de.ubt.ai4.petter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.camunda.bpm.engine.task.Task;

import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsTask {

    private Long id;
    private String taskId;
    private String assignee;
    private String activityId;
    private String processInstanceId;
    private String processDefinitionId;
    private int priority;
    private Instant createDate;
    private Instant dueDate;
    private String executionId;

    public static BpmsTask fromTask(Task task) {
        if (task.getDueDate() != null) {
            return new BpmsTask(
                    -1L,
                    task.getId(),
                    task.getAssignee(),
                    task.getTaskDefinitionKey(),
                    task.getProcessInstanceId(),
                    task.getProcessDefinitionId(),
                    task.getPriority(),
                    task.getCreateTime().toInstant(),
                    task.getDueDate().toInstant(),
                    task.getExecutionId()
            );
        } else {
            return new BpmsTask(
                    -1L,
                    task.getId(),
                    task.getAssignee(),
                    task.getTaskDefinitionKey(),
                    task.getProcessInstanceId(),
                    task.getProcessDefinitionId(),
                    task.getPriority(),
                    task.getCreateTime().toInstant(),
                    null,
                    task.getExecutionId()
            );
        }

    }

    public static List<BpmsTask> fromTaskList(List<Task> taskList) {
        return taskList.stream().map(BpmsTask::fromTask).toList();
    }
}
