package de.ubt.ai4.petter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.ProcessDefinition;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsProcess {
    private String id;
    private String name;
    private String description;
    private String version;
    private String key;
    private boolean latest;
    private BpmsProcessModel processModel;

    public static BpmsProcess fromProcessDefinitionAndRepositoryService(ProcessDefinition processDefinition, RepositoryService repositoryService, List<ProcessDefinition> latest) {
        return new BpmsProcess(
                processDefinition.getId(),
                processDefinition.getName(),
                processDefinition.getDescription(),
                processDefinition.getVersionTag(),
                processDefinition.getKey(),
                latest.stream().anyMatch(definition -> processDefinition.getId().equals(definition.getId())),
                BpmsProcessModel.fromProcessDefinition(processDefinition, repositoryService)
        );
    }

    public static List<BpmsProcess> fromProcessList(List<ProcessDefinition> processList, RepositoryService repositoryService, List<ProcessDefinition> latest) {
        return processList.stream().map(processDefinition -> BpmsProcess.fromProcessDefinitionAndRepositoryService(processDefinition, repositoryService, latest)).toList();
    }
}
