package de.ubt.ai4.petter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.camunda.bpm.model.bpmn.instance.UserTask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsActivity {
    private String id;
    private String name;
    private String type;
    private List<BpmsRole> roles = new ArrayList<>();

    public static BpmsActivity getFromUsertask(UserTask userTask) {
        return new BpmsActivity(
                userTask.getId(),
                userTask.getName(),
                userTask.getElementType().getTypeName(),
                BpmsRole.getFromUserTask(userTask)
        );
    }

    public static List<BpmsActivity> getFromFlowElementCollection(Collection<UserTask> flowElements) {
        return flowElements.stream().map(BpmsActivity::getFromUsertask).toList();
    }
}
