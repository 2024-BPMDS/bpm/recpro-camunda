package de.ubt.ai4.petter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.impl.instance.ChildLaneSet;
import org.camunda.bpm.model.bpmn.instance.Lane;
import org.camunda.bpm.model.bpmn.instance.UserTask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsRole {
    private String id;
    private String name;
    private List<BpmsRole> childElements = new ArrayList<>();

    public static BpmsRole getFromLane(Lane lane) {
        ChildLaneSet laneSet = lane.getChildLaneSet();
        if (laneSet != null) {
            Collection<Lane> lanes = lane.getChildLaneSet().getLanes();
            return new BpmsRole(
                    lane.getId(),
                    lane.getName(),
                    lanes.stream().map(BpmsRole::getFromLane).toList()
            );
        } else {
            return new BpmsRole(
                    lane.getId(),
                    lane.getName(),
                    new ArrayList<>()
            );
        }
    }

    public static BpmsRole getFromString(String role) {
        return new BpmsRole(
                role, role, new ArrayList<>()
        );
    }

    public static List<BpmsRole> getFromModelInstance(BpmnModelInstance model) {
        Collection<Lane> lanes = model.getModelElementsByType(Lane.class);
        return lanes.stream().map(BpmsRole::getFromLane).toList();
    }

    public static List<BpmsRole> getFromUserTask(UserTask userTask) {
        return userTask.getCamundaCandidateGroupsList().stream().map(BpmsRole::getFromString).toList();
    }
}