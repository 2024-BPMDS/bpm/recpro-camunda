package de.ubt.ai4.petter.execution.task;

import de.ubt.ai4.petter.model.BpmsTask;
import lombok.AllArgsConstructor;
import org.camunda.bpm.engine.task.Task;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@AllArgsConstructor
public class RecproTaskService {

    private org.camunda.bpm.engine.TaskService taskService;

    public BpmsTask getById(String taskId) {
        return BpmsTask.fromTask(this.getTaskById(taskId));
    }

    private Task getTaskById(String taskId) {
        return taskService.createTaskQuery().taskId(taskId).initializeFormKeys().singleResult();
    }

    private void saveTask(Task task) {
        this.taskService.saveTask(task);
    }

    public void claim(String taskId, String userId) {
        this.assignTask(taskId, userId);
    }

    public void completeTask(String taskId) {
        taskService.complete(taskId);
    }

    public void deleteTask(String taskId) {
        taskService.deleteTask(taskId);
    }

    public void assignTask(String taskId, String userId) {
        taskService.setAssignee(taskId, userId);
    }

    public void unclaimTask(String taskId) {
        this.assignTask(taskId, null);
    }

    public void setPriority(String taskId, int priority) {
        Task task = this.getTaskById(taskId);
        task.setPriority(priority);
        this.saveTask(task);
    }

    public void setAttribute(String taskId, String attributeId, Object value) {
        taskService.setVariable(taskId, attributeId, value);
    }

    public void setAttributes(String taskId, Map<String, Object> attributes) {
        taskService.setVariables(taskId, attributes);
    }
}
