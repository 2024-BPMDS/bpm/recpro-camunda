package de.ubt.ai4.petter.execution.process;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/api/execution/process/")
@AllArgsConstructor
public class ProcessController {

    private ProcessService processService;

    @PostMapping("start/{processId}")
    public void startProcessById(@PathVariable String processId) {
        processService.startProcess(processId);
    }

    @PostMapping("setAttribute/{processInstanceId}")
    public void setAttribute(@RequestParam String attributeId, @PathVariable String processInstanceId, @RequestParam  Object value) {
        processService.setAttribute(attributeId, processInstanceId, value);
    }

    @PostMapping("setAttributes/{processInstanceId}")
    public void setAttributes(@PathVariable String processInstanceId, @RequestBody Map<String, Object> attributes) {
        processService.setAttributes(processInstanceId, attributes);
    }
}
