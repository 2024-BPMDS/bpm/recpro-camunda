package de.ubt.ai4.petter.execution.tasklist;

import de.ubt.ai4.petter.model.BpmsTask;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/execution/tasklist/")
@AllArgsConstructor
public class TasklistController {

    private TasklistService tasklistService;

    @GetMapping("getByAssignee")
    public List<BpmsTask> getByAssignee(@RequestParam String assigneeId) {
        return tasklistService.getUserTasks(assigneeId);
    }

    @GetMapping("getAll")
    public List<BpmsTask> getAllTasks() {
        return tasklistService.getAll();
    }
}
