package de.ubt.ai4.petter.execution.tasklist;

import de.ubt.ai4.petter.model.BpmsTask;
import lombok.AllArgsConstructor;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.task.Task;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@AllArgsConstructor
public class TasklistService {

    private final TaskService taskService;

    public List<BpmsTask> getUserTasks(String assignee) {
        List<Task> taskList = taskService.createTaskQuery().taskAssignee(assignee).list();
        return BpmsTask.fromTaskList(taskList);
    }

    public List<BpmsTask> getAll() {
        List<Task> taskList = taskService.createTaskQuery().initializeFormKeys().list();
        return BpmsTask.fromTaskList(taskList);
    }
}
