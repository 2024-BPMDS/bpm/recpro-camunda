package de.ubt.ai4.petter.execution.process;

import lombok.AllArgsConstructor;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@AllArgsConstructor
public class ProcessService {
    private RuntimeService runtimeService;

    public void startProcess(String processId) {
        runtimeService.startProcessInstanceById(processId);
    }

    public void setAttribute(String attributeId, String processInstanceId, Object value) {
        runtimeService.setVariable(processInstanceId, attributeId, value);
    }

    public void setAttributes(String processInstanceId, Map<String, Object> attributes) {
        runtimeService.setVariables(processInstanceId, attributes);
    }
}
